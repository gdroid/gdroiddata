#!/bin/bash
mkdir env
virtualenv env/
. env/bin/activate
env/bin/pip install pyyaml
pip install numpy
pip install python-Levenshtein
pip install requests
